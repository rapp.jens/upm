#!/usr/bin/bash

if [ $#  -ne 1 ]; then
	echo "usage:"
	echo "$0 repository (pypi,testpypi,...)"
	exit 1
fi

repository=$1

if [ "$repository" == "pypi" ] ; then
	git commit -a -m 'deploying'
	git push origin master
fi

python3 -m twine upload --verbose --repository $repository dist/*.whl
