#!/bin/bash
# remove old dist directory
rm -rf dist
# build
python -m build
# install locally
pip install --force-reinstall --break-system-packages dist/upm*.whl
